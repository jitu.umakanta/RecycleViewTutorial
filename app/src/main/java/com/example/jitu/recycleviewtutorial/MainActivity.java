package com.example.jitu.recycleviewtutorial;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;

import java.util.ArrayList;
import java.util.List;

import static com.example.jitu.recycleviewtutorial.data.GetData.initBookData;

public class MainActivity extends AppCompatActivity {
    private List<Book> bookList = new ArrayList<>();
    private RecyclerView recyclerView;
    private BookAdapter mAdapter;
    private ImageView imageView;
    public NetworkImageView imageView1;
    RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView();

        initdata();

    }

    public void initView() {
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        // imageView1 = (NetworkImageView) imageView.findViewById(R.id.imageView);

        mAdapter = new BookAdapter(bookList, this);

        /* set the layout manager */
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setLayoutAnimation(animation);

        // recyclerView.addItemDecoration(new ItemDividerDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getApplicationContext(), recyclerView, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                Toast.makeText(getApplicationContext(), bookList.get(position).getTitle() + position + " is clicked!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(getApplicationContext(), bookList.get(position).getTitle() + " is long pressed!", Toast.LENGTH_SHORT).show();

            }
        }));
        imageView = (ImageView) findViewById(R.id.imageView);
    }

    public void initdata() {
        //bookList=initBookData();

        for(Book b:initBookData()){
            bookList.add(b);
        }



        System.out.println("sixe of bookList: "+bookList.size());
        System.out.println("bookList.get(2)"+bookList.get(2));
        if(bookList.size()!=0) {
            mAdapter.notifyDataSetChanged();
        }else{
            Toast.makeText(this, "size is zero", Toast.LENGTH_SHORT).show();
        }
    }




    public void imagereq() {
        //imageView1.setImageUrl();
    }
}
