package com.example.jitu.recycleviewtutorial.data;

import com.example.jitu.recycleviewtutorial.Book;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GetData {
    public static ArrayList<Book> initBookData() {
       /* Book book = new Book("Hello Android", "Ed Burnette");
        bookList.add(book);

        book = new Book("Beginning Android 3", "Mark Murphy");
        bookList.add(book);

        book = new Book("Unlocking Android", " W. Frank Ableson");
        bookList.add(book);

        book = new Book("Android Tablet Development", "Wei Meng Lee");
        bookList.add(book);

        book = new Book("Android Apps Security", "Sheran Gunasekera");
        bookList.add(book);

        book = new Book("Android Apps Security", "Sheran Gunasekera");
        bookList.add(book);

        book = new Book("Android Apps Security", "Sheran Gunasekera");
        bookList.add(book);

        book = new Book("Android Apps Security", "Sheran Gunasekera");
        bookList.add(book);

        book = new Book("Android Apps Security", "Sheran Gunasekera");
        bookList.add(book);*/

        ArrayList<Book> bookList=new ArrayList<>();
        for (int i = 1; i < 100; i++) {
            Book b = new Book("Title: ", " Author: " + i);
            bookList.add(b);
        }

        return bookList;
    }
}
